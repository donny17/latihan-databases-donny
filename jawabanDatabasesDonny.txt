1 Membuat Database myshop
CREATE DATABASE myshop;

2. Membuat Tabel

- Tabel users
CREATE TABLE users( id int(4) AUTO_INCREMENT PRIMARY KEY, name varchar (255) NOT null , email varchar (255) NOT null , password varchar (255) );

- Tabel categories
CREATE TABLE categories( id int PRIMARY KEY AUTO_INCREMENT, name varchar(255) NOT null );

- Tabel items
CREATE TABLE items( id int AUTO_INCREMENT PRIMARY KEY, name varchar(255) NOT null, description varchar(255), price int , stock int, category_id int, FOREIGN KEY(category_id) REFERENCES categories(id) );

3. Insert Data
- Tabel users 
INSERT INTO users(name, email, PASSWORD) VALUES ("John Doe", "john@doe.com", "john123"),("Jane Doe", "jane@doe.com", "jenita123");

- Tabel categories
INSERT INTO categories(name) VALUES ("gadget"), ("cloth"), ("men"), ("women"), ("branded");

- Tabel items
INSERT INTO items(name, description, price, stock, category_id) VALUES ("Sumsang b50", "hape keren dari merek sumsang", 4000000, 100, 1), ("Uniklooh", "baju keren dari brand ternama", 500000, 50, 2), ("IMHO Watch", "jam tangan anak yang jujur banget", 2000000,10, 1);

4 Mengambil Data dari Database
a. Mengambil data Tabel users
SELECT name, email FROM users;

b. Tabel Items
 - Harga diatas 1000000
 SELECT * FROM items WHERE price > 1000000;
 
 - Yang memiliki name serupa atau mirip (like) dengan kata kunci “uniklo”,
 SELECT * FROM items WHERE name LIKE "uniklo%";
 
c. Menampilkan data items join dengan kategori
 SELECT items.name, items.description, items.price, items.stock,	items.category_id, categories.name AS kategori from items INNER JOIN categories on items.category_id = categories.id;
 
 
5. Mengubah Data dari Database nama sumsang b50 harganya (price) menjadi 2500000
UPDATE items set price=2500000 WHERE id = 1; 

 
 